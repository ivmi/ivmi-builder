extends "res://addons/ivmi-builder/techniques/manipulation/HOMER.gd"

var _looping =false

func _input(event):
	if event is InputEventMouseButton :
		if event.button_index==MOUSE_BUTTON_RIGHT:
			if _selected:
				if event.is_pressed():
					_selected.set_property("looped", [1])
				else:
					_selected.set_property("looped", [0])
		elif event.button_index==MOUSE_BUTTON_LEFT:
			if _selected:
				_selected.set_property("looped", [-1])

func _on_HOMER_button_pressed(button):
	super._on_HOMER_button_pressed(button)
	if button == JOY_BUTTON_A :
		if _selected:
			_selected.set_property("looped", [1])

func _on_HOMER_button_release(button):
	super._on_HOMER_button_release(button)
	if button == JOY_BUTTON_A :
		if _selected:
			_selected.set_property("looped", [0])
