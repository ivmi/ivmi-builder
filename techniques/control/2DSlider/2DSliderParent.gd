extends "res://addons/ivmi-builder/techniques/technique.gd"

@onready var _pos_x_1 = get_node("Position3D_X_1")
@onready var _pos_x_2 = get_node("Position3D_X_2")
@onready var _pos_y_1 = get_node("Position3D_Y_1")
@onready var _pos_y_2 = get_node("Position3D_Y_2")

@onready var _cursor = get_node("Cursor")

func _ready():
	_add_property("value",[0,0])
	
func _set_property(prop, vals) :
	super._set_property(prop, vals)
	match prop:
		"value":
			var _value = _cursor.get_property("value")
			if !(_value[0] == vals[0] and _value[1] == vals[1]):
				_cursor.set_property("value", vals)
			_pos_x_1.position.y = _cursor.position.y
			_pos_x_2.position.y = _cursor.position.y
			_pos_y_1.position.x = _cursor.position.x
			_pos_y_2.position.x = _cursor.position.x
