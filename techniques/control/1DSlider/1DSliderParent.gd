extends "res://addons/ivmi-builder/techniques/technique.gd"

@onready var _cursor = get_node("Cursor")

func _ready():
	_add_property("value",[0])
	
func _set_property(prop, vals) :
	super._set_property(prop, vals)
	match prop:
		"value":
			var _value = _cursor.get_property("value")
			if !(_value[0] == vals[0]):
				_cursor.set_property("value", vals)
