extends "res://addons/ivmi-builder/core/IvmiNode.gd"

@onready var _parent = get_parent()

func get_extent():
	return Vector3(2.0, 2.0, 1)
	
func _ready():
	_add_property("value",[0])
	_can_be_moved = false
	
func set_property(prop, vals) :
	super.set_property(prop, vals)
	match prop:
		"quaternion":
			rotation_degrees.x = 0
			rotation_degrees.y = 0
		"rotation":
			rotation_degrees.x = 0
			rotation_degrees.y = 0
			_parent.set_property("value",[vals[0]/360])
		"value":
			set_property("rotation",[vals[0]*360,0,0])
			_parent.set_property("value",[vals[0]])
