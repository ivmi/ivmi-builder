extends IvmiNode

@export var _speed_evaluation_delay = 0.1
@onready var __last_pos = global_transform.origin

var _speed_evaluation_time = 0

func _ready():
	super._ready()
	_set_ivmi_type("technique")
	add_to_group("ivmi_techniques")
	_add_property("speed", [0])

func _process(delta):
	super._process(delta)
	if _properties["speed"]._listen:
		_speed_evaluation_time += delta
		if _speed_evaluation_time >= _speed_evaluation_delay:
			var _speed = (global_transform.origin - __last_pos).length()
			set_property("speed",[_speed])
			_speed_evaluation_time = 0
