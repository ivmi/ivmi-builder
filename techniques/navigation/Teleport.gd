extends "res://addons/ivmi-builder/techniques/technique.gd"

@export var _controller_node_path: NodePath
@export var _camera_node_path: NodePath
@export var _arvr_origin_node_path: NodePath

@onready var _controller = get_node(_controller_node_path)
@onready var _camera = get_node(_camera_node_path)
@onready var _arvr_origin = get_node(_arvr_origin_node_path)
@onready var _raycast = get_node("MeshInstance3D/RayCast3D")
@onready var _target = get_node("Target")



func _ready():
	_controller.connect("button_pressed",Callable(self,"_on_HOMER_button_pressed"))
	_controller.connect("button_released",Callable(self,"_on_HOMER_button_release"))

func _physics_process(delta):
	
	if _raycast.is_colliding():
		var target_pos = _raycast.get_collision_point()
		_target.global_transform.origin = target_pos

func _teleport():
	_arvr_origin.position = _target.position
		

func _on_Controller_button_pressed(button):
	if button == JOY_VR_TRIGGER :
		_teleport()
	
func _on_Controller_button_released(button):
 pass
