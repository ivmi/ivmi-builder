extends "res://addons/ivmi-builder/techniques/technique.gd"

@export var _controller_node_path: NodePath
@export var _camera_node_path: NodePath
@export var _mouse_sensitivity = 1
@export var _scroll_sensitivity = 0.04
@export var _max_mouse_speed = 0.5

@onready var _controller = get_node(_controller_node_path)
@onready var _camera = get_node(_camera_node_path)
@onready var _last_pos = position
#onready var _last_rotation = rotation_degrees

var _squared_max_mouse_speed = pow(_max_mouse_speed,2)
var _selected = {}
var _grabbed = {}
var _grabbing = false

var _move_diff
var _mouse_delta = Vector2(0,0)
var _cursor_can_be_moved = true

func _ready():
	_controller.connect("button_pressed",Callable(self,"_on_Controller_button_pressed"))
	_controller.connect("button_released",Callable(self,"_on_Controller_button_release"))

func _input(event):
	if event is InputEventMouseMotion :
		_mouse_delta = event.relative
	if event is InputEventMouseButton:
		if event.is_pressed():
			var _z_movement = _scroll_sensitivity * _camera.transform.basis.z.normalized()
			if event.button_index == MOUSE_BUTTON_WHEEL_DOWN :
				_controller.position -= _z_movement
			if event.button_index == MOUSE_BUTTON_WHEEL_UP :
				_controller.position += _z_movement
			if  event.is_action("grab"):
				_grabbing = true
				for k in _selected.keys():
					_grabbed[k] = true
			if event.is_action("trigger"):
				for k in _selected.keys():
					_selected[k].set_property("triggered", [1])
		else:
			if  event.is_action("grab"):
				_grabbing = false
				for k in _selected.keys():
					_grabbed[k] = false
			if event.is_action("trigger"):
				for k in _selected.keys():
					_selected[k].set_property("triggered", [0])
#	if event is InputEventKey:
#		if event.
#			var _mouse_delta = Vector2(0,0)
#			_cursor_can_be_moved = event.is_pressed()

func _physics_process(delta):
	if _ivmi._is_2D and _cursor_can_be_moved:
		_move_controller(delta)
	_move_cursor()
	if _grabbing:
		_move_grabbed_object()
		_rotate_grabbed_object()
	_last_pos = global_transform.origin

func _move_controller(delta):
	var _mouse_speed = delta * _mouse_delta
	if _mouse_speed.length_squared() <= _squared_max_mouse_speed:
		var _coef = delta*_mouse_sensitivity
		_controller.position += _coef*_mouse_delta.x*_camera.transform.basis.x.normalized()
		_controller.position -= _coef*_mouse_delta.y*_camera.transform.basis.y.normalized()
	_mouse_delta = Vector2.ZERO

func _move_cursor():
	position = _controller.position	

func _move_grabbed_object():
	_move_diff = global_transform.origin - _last_pos
	for k in _selected.keys():
		if _grabbed[k]:
			_selected[k].set_property("position",_ivmi.vector3_to_array(_selected[k].position+_move_diff))

func _rotate_grabbed_object():
	pass
	#_move_diff = global_transform.origin - _last_pos
	#for k in _selected.keys():
	#	if _grabbed[k]:
	#		_selected[k].set_property("position",(_selected[k].position+_move_diff))

func _on_Area_body_entered(body):
	_on_node_entered(body)
func _on_Area_body_exited(body):
	_on_node_exited(body)
func _on_Area_area_entered(area):
	_on_node_entered(area)
func _on_Area_area_exited(area):
	_on_node_exited(area)

func _on_node_exited(node):
	var _parent = node.get_parent()
	if _selected.has(_parent.name):
		_selected[_parent.name].set_property("selected", [0])
		_selected.erase(_parent.name)
		_grabbed.erase(_parent.name)


func _on_node_entered(node):
	var _parent = node.get_parent()
	if _parent.is_in_group("ivmi_nodes"):
		_selected[_parent.name] = _parent
		_selected[_parent.name].set_property("selected", [1])
		_grabbed[_parent.name] = false

func _on_Controller_button_pressed(button):
	if button == "grip_click" :
		_grabbing = true
		for k in _selected.keys():
			_grabbed[k] = true
	elif button == "trigger_click" :
		for k in _selected.keys():
			_selected[k].set_property("triggered", [1])
	
func _on_Controller_button_release(button):
	if button == "grip_click" :
		_grabbing = false
		for k in _selected.keys():
			_grabbed[k] = false
	elif button == "trigger_click" :
		for k in _selected.keys():
			_selected[k].set_property("triggered", [0])



