extends "res://addons/ivmi-builder/techniques/technique.gd"

@export var _controller_node_path: NodePath
@export var _camera_node_path: NodePath
@export var _arvr_origin_node_path: NodePath

@onready var _controller = get_node(_controller_node_path)
@onready var _camera = get_node(_camera_node_path)
@onready var _arvr_origin = get_node(_arvr_origin_node_path)
@onready var _raycast = get_node("ray/RayCast3D")
@onready var _ray = get_node("ray")

@export var _grab_sensibility = 5.0
@export var _mouse_sensitivity = 1.0
@export var _scroll_sensitivity = 0.04

var _selected = null
var _grabbed = null

# Represent the current object targeted (which mean selected or grabbed)
var _target = null

var _grabbing = false
var _highlight = null

var _mouse_move_diff = Vector3.ZERO 
var _mouse_move_start = Vector3.ZERO 
var _grabbed_depth_translation = Vector3.ZERO

var _start_controller_global_origin
var _start_arvr_global_origin
var _start_grabbed_local_translation

var _start_controller_global_quaternion
var _start_arvr_global_quaternion
var _start_grabbed_local_quaternion

var _rotating_clockwise = false
var _rotating_counter_clockwise = false

func _ready():
	super._ready()
	_controller.connect("button_pressed",Callable(self,"_on_HOMER_button_pressed"))
	_controller.connect("button_released",Callable(self,"_on_HOMER_button_release"))

	_highlight = get_node("highlight")
	_highlight.set_as_top_level(true)

func _physics_process(delta):
	# Rotate and move homer to match the position and
	# rotation of the controller
	position = _controller.position
	rotation = _controller.rotation

	if _grabbed:
		_grab_update(delta)
	else:
		_select_update(delta)

	if _target:
		_highlight.global_transform.origin = _target.global_transform.origin
		_highlight.global_transform.basis = _target.global_transform.basis
		_highlight.set_scale(_target.get_extent())

func _grab_update(delta):
	var _diff_arvr_global_origin = _arvr_origin.global_transform.origin - _start_arvr_global_origin
	
	if _ivmi.is_2D():
		var new_grabbed_position = _start_grabbed_local_translation - _mouse_move_diff + _grabbed_depth_translation + _diff_arvr_global_origin
		_grabbed.set_property("position",_ivmi.vector3_to_array(new_grabbed_position))
		if _rotating_clockwise:
			var new_rot = _grabbed.transform.basis.rotated(_camera.global_transform.basis.z.normalized(), PI*delta).get_rotation_quaternion()
			_grabbed.set_property("quaternion",[new_rot.x,new_rot.y,new_rot.z,new_rot.w])
		if _rotating_counter_clockwise:
			var new_rot = _grabbed.transform.basis.rotated(_camera.global_transform.basis.z.normalized(), -PI*delta).get_rotation_quaternion()
			_grabbed.set_property("quaternion",[new_rot.x,new_rot.y,new_rot.z,new_rot.w])
	else:
		## MOVEMENT
		var _diff_controller_global_origin = _controller.global_transform.origin - _start_controller_global_origin
		# Subtract ar vr origin delta
		_diff_controller_global_origin -= _diff_arvr_global_origin
		# Apply multiply by constant
		_diff_controller_global_origin *= _grab_sensibility
		
		var new_grabbed_position = _start_grabbed_local_translation + _diff_controller_global_origin + _diff_arvr_global_origin
		_grabbed.set_property("position",_ivmi.vector3_to_array(new_grabbed_position))
		
		##ROTATION
		var _diff_arvr_global_quaternion = _start_arvr_global_quaternion * _arvr_origin.global_transform.basis.get_rotation_quaternion().inverse()
		var _diff_controller_global_quaternion = _start_arvr_global_quaternion * _controller.global_transform.basis.get_rotation_quaternion().inverse()
		
		var new_grabbed_quat = _start_grabbed_local_quaternion * _diff_arvr_global_quaternion * _diff_controller_global_quaternion
		new_grabbed_quat = new_grabbed_quat.inverse()
		_grabbed.set_property("quaternion",[new_grabbed_quat.x,new_grabbed_quat.y,new_grabbed_quat.z,new_grabbed_quat.w])
		


func _select_update(delta):
	var result = _raycast.get_collider()
	if result and result.get_parent().is_in_group("ivmi_nodes") \
		and result.get_parent()._can_be_selected:
				
		var obj = result.get_parent()
		var _selection_changed = false
		
		# place selection sphere and change ray size
		var pnt = _raycast.get_collision_point()
		$SelectionSphere.global_position = pnt
		$SelectionSphere.visible = true
		var dist = (_raycast.global_position).distance_to(pnt)
		$ray.scale.z = dist
		
		
		#update selected if needed
		if _selected and _selected!=obj:
			_selected.set_property("selected", [0])
			_selection_changed = true
		if _selected == null:
			obj.set_property("selected", [1])
			_selection_changed = true
		_selected = obj
		_target = obj

#		#Update highlight
#		if _selection_changed:
#			_highlight.global_transform.basis = _selected.global_transform.basis
#			_highlight.set_scale(_selected.get_extent())
#			_highlight.visible=true

		# Started grabbing
		if _grabbing :
			# switch to manipulation mode
			_ray.visible = false
			_grabbed = _selected
			_grabbed.set_property("grabbed", [1])
			#Starting pos of the selected object
			_start_controller_global_origin = _controller.global_transform.origin
			_start_arvr_global_origin  = _arvr_origin.global_transform.origin
			_start_grabbed_local_translation  = _grabbed.position
			#Startiong rotation of the selected object
			_start_controller_global_quaternion = _controller.global_transform.basis.get_rotation_quaternion()
			_start_arvr_global_quaternion = _arvr_origin.global_transform.basis.get_rotation_quaternion()
			_start_grabbed_local_quaternion = _grabbed.transform.basis.get_rotation_quaternion().inverse()
			if _ivmi.is_2D():
				#Starting pos of the mouse
				_mouse_move_start = ((_controller.get_viewport().get_mouse_position()/Vector2(_controller.get_viewport().size))-Vector2(0.5,0.5))/2.0
				_mouse_move_diff = Vector3.ZERO
				_grabbed_depth_translation = Vector3.ZERO
	else :
		if _selected:
			_selected.set_property("selected", [0])
			_selected = null
			_target = null
			_highlight.visible=false
		$SelectionSphere.visible = false
		$ray.scale.z = 10.0

func _unhandled_input(event):
	if _ivmi.is_2D():
		if event is InputEventMouseMotion :
			
			#move the ray
			var rot = ((event.position/Vector2(_controller.get_viewport().size))-Vector2(0.5,0.5))*2.0
			_controller.rotation_degrees.y = -rot.x*90.0
			_controller.rotation_degrees.x = -rot.y*90.0
			
			#move selected object according to new position
			if _grabbed :
				var pos = ((event.position/Vector2(_controller.get_viewport().size))-Vector2(0.5,0.5))*2.0
				_mouse_move_diff = Vector3.ZERO
				_mouse_move_diff += -(pos.x-_mouse_move_start.x)*_camera.global_transform.basis.x.normalized()*_mouse_sensitivity
				_mouse_move_diff += (pos.y- _mouse_move_start.y)*_camera.global_transform.basis.y.normalized()*_mouse_sensitivity

		elif event is InputEventMouseButton :
			if event.button_index == MOUSE_BUTTON_LEFT or event.button_index == MOUSE_BUTTON_RIGHT :
				_grabbing = event.pressed
				if _grabbed:
					_grabbed.set_property("grabbed", [0])
					_grabbed=null
					#back to select mode
					_ray.visible=true
			if event.button_index == MOUSE_BUTTON_RIGHT :
				if _selected:
					if event.is_pressed():
						_selected.set_property("triggered", [1])
					else:
						_selected.set_property("triggered", [0])
			if event.is_pressed() and _grabbed:
				var _z_movement = _scroll_sensitivity*(_camera.global_transform.origin-_grabbed.global_transform.origin).normalized()
				if event.button_index == MOUSE_BUTTON_WHEEL_DOWN :
					_grabbed_depth_translation -= _z_movement
				if event.button_index == MOUSE_BUTTON_WHEEL_UP :
					_grabbed_depth_translation += _z_movement
		elif event is InputEventKey:
			if event.keycode == KEY_LEFT :
				_rotating_clockwise = event.is_pressed()
			if event.keycode == KEY_RIGHT :
				_rotating_counter_clockwise = event.is_pressed()

func _on_HOMER_button_pressed(button):
	if button == "Grip":
		_grabbing = true
	if button == "Trigger":
		if _selected:
			_selected.set_property("triggered", [1])

func _on_HOMER_button_release(button):
	if button == "Grip" :
		_grabbing = false
		if _grabbed:
			_grabbed.set_property("grabbed", [0])
			_grabbed=null
			#back to select mode
			_ray.visible=true
	if button == "Trigger" :
		if _selected:
			_selected.set_property("triggered", [0])
