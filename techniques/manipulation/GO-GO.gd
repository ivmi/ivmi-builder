extends "res://addons/ivmi-builder/techniques/manipulation/Cursor.gd"

var _body_offset = Vector3(0,-0.20,0)
@export var gogo_distance = 0.450
@export var gogo_multiplier = 100

func _move_cursor():
	if _controller:
		if !_controller.position.is_equal_approx (Vector3.ZERO):
			var _body_controller_vec = _controller.position - (_camera.position + _body_offset)
			var _slen = _body_controller_vec.length()
			if _slen > gogo_distance:
				position = _controller.position + _body_controller_vec*gogo_multiplier*pow(_slen - gogo_distance,2)
			else:
				position = _controller.position
