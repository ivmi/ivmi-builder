extends Node3D
class_name IvmiScene

@export_group("PureData")
enum PdMode {OSC, LIBPD, NONE}
@export var _pd_mode : PdMode = PdMode.OSC

#OSC variables
@export var _pd_osc_address : String = "239.215.216.218"
var _input_port = 8215
var _discov_input_port = 8216
var _discov_input_address = "239.215.216.217"
var _osc_output_port = 8217
var _osc_to_pd : GodOSC
var _osc_discov : GodOSC

#PD variables
@export_file("*.pd") var _pd_patch
@export var _rt_audio : bool = false
var _gdpd

@export var _debug_osc: bool = false

#XR variables
@export_group("XR")
enum XRMode {Mono, OpenXR}
@export var _xr_mode: XRMode = XRMode.OpenXR
@export var _open_xr_passthrough: bool=false

var _xr_interface : XRInterface = null

# Network variables
@export_group("Network")
enum NetMode {None, Peer, Client, Server}
@export var _network_mode: NetMode = NetMode.None
enum NetProto {Enet}
var _network_protocol: NetProto = NetProto.Enet
var _client_name = ""
var _is_connected = false
@export var _server_ip = "127.0.0.1" : set = set_server_ip
@export var _server_port = 7596 : set = set_server_port
#export var _max_player = 10 : set = set_max_player
var player_info = {} # all players infos ( { id: { name } } )
var my_info = {} # current player info ( { name } )
var _player_count = 0
var _network_discov
var _network_client
var _network_server

# Recording
enum RecordingState {STOPPED, RECORDING, PLAYING}
var _recording_state = RecordingState.STOPPED
var _recorded_props = []
var _recording_time = 0
var _recording_index = 0


var _is_2D : bool = true
var _ivmi_node = load("res://addons/ivmi-builder/core/IvmiNode.gd")

func _ready():
	
	print("IVMI : Creating IvmiScene")
	
	_is_2D=true
	match _xr_mode :
		XRMode.OpenXR :
			_xr_interface = XRServer.find_interface("OpenXR")
			if _xr_interface and _xr_interface.initialize():
				#remove v-sync
				DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_DISABLED)
				get_viewport().use_xr = true
				_xr_interface.connect("session_visible",_on_xr_session)
				_is_2D = false
				print("IVMI : Initialised OpenXR Interface")
				set_passthrough(_open_xr_passthrough)
			else:
					print("Error : Could not initialize OpenXR interface")

	# PureData mode
	if _pd_mode==PdMode.LIBPD :
		var gdpdir = DirAccess.open("res://addons/gdpd")
		var gdpdok : bool = false
		if gdpdir :
			_gdpd = ClassDB.instantiate("GdPd")
			add_child(_gdpd)
			if _pd_patch!="" :
				# Get all input and output devices
				var inps = _gdpd.get_available_input_devices()
				var outs = _gdpd.get_available_output_devices()
				if inps.size()>0 and outs.size()>0 :
					# Initialise the first ones
					_gdpd.init_devices(inps[0], outs[0])
					# Open patch
					var global_patch = ProjectSettings.globalize_path(_pd_patch)
					var patch_name = global_patch.split("/")[-1]
					var patch_dir = global_patch.trim_suffix(patch_name)
					gdpdok=_gdpd.openfile(patch_name, patch_dir)
		
		if not gdpdok :
			print("IVMI : Could not load gdpd addon")
			_pd_mode=PdMode.OSC

	if _pd_mode==PdMode.OSC :
		# output
		_osc_to_pd = GodOSC.new()
		_osc_to_pd.set_output(_pd_osc_address, _osc_output_port)

		# input
		_osc_discov = GodOSC.new()
		_osc_discov.set_input_port(_discov_input_port)
		_osc_discov.set_multicast(_discov_input_address)

	#initialize either libpd or osc patch
	send("init", "f", [1])

	# Start network
	_start_network()


func _process(delta) :
	if is_inside_tree() :
		match _pd_mode :
			PdMode.OSC :
				#process osc messages
				#while _osc_to_pd.has_msg() :
					#var msg = _osc_to_pd.get_msg()
					#_parse_message(msg)

				while _osc_discov.has_msg() :
					var msg = _osc_discov.get_msg()
					_parse_message(msg)
					#print("from discov ", msg, _osc_discov.get_last_peer())
					#match msg["address"] :
						#"/ivmi/hello_from_pd" :
							#var peer = _osc_discov.get_last_peer()
							##print(_output_address, _output_port)
							#if _osc_output_address!=peer["address"] or _osc_output_port!=peer["port"] :
								#_osc_output_address = peer["address"]
								#_osc_output_port = peer["port"]
								#_osc_to_pd.set_output(_osc_output_address, _osc_output_port)
								#var local_addr = _osc_to_pd.get_local_address()
								#_osc_to_pd.send_msg("/ivmi/hello_from_gd","sf",[local_addr, _input_port])

			PdMode.LIBPD : #libpd mode
				while _gdpd.has_message() :
					var list = _gdpd.get_next()
					var msg = {}
					msg["address"]=list[0]
					list.pop_front()
					msg["args"]=list
					_parse_message(msg)

		if _recording_state == RecordingState.PLAYING :
			var t = Time.get_ticks_msec() - _recording_time
			while _recording_index<_recorded_props.size() and _recorded_props[_recording_index]["time"] < t :
				_parse_message(_recorded_props[_recording_index])
				_recording_index+=1
			if _recording_index>=_recorded_props.size():
				_recording_state=RecordingState.STOPPED
				_recording_playing_done()



# --------XR-----------------

func set_passthrough(activate : bool) -> void :
	if  _xr_interface :
		get_viewport().transparent_bg=true
		var pt : bool = true
		if _xr_interface.is_passthrough_supported():
			if activate : 
				if !_xr_interface.start_passthrough():
					pt=false
			else :
				_xr_interface.stop_passthrough()
		else:
			if activate : 
				var modes = _xr_interface.get_supported_environment_blend_modes()
				if _xr_interface.XR_ENV_BLEND_MODE_ALPHA_BLEND in modes:
					_xr_interface.set_environment_blend_mode(_xr_interface.XR_ENV_BLEND_MODE_ALPHA_BLEND)
				else:
					pt=false
			else :
				_xr_interface.set_environment_blend_mode(_xr_interface.XR_ENV_BLEND_MODE_OPAQUE)

		if activate :
			if pt :
				print("IVMI : Activated OpenXR Passthrough")
			else:
				print("Error : Could not activate OpenXR Passthrough")
		else :
			print("IVMI : Deactivated OpenXR Passthrough")

func _on_xr_session() :
	get_viewport().use_xr = true
	_start_network()

# --------Network-----------------

func _start_network() -> void :
	_is_connected=false
	
	if _network_mode!=NetMode.None :
		if _network_protocol==NetProto.Enet :
			match _network_mode :
				NetMode.Server :
					# init server
					var peer = ENetMultiplayerPeer.new()
					peer.create_server(_server_port)
					multiplayer.multiplayer_peer = peer
					multiplayer.multiplayer_peer.peer_connected.connect(_on_peer_connected)
					multiplayer.multiplayer_peer.peer_disconnected.connect(_on_peer_disconnected)
					_on_network_ready()
					print("IVMI : Starting Server")
				NetMode.Client :
					if _server_ip!="" :
						_on_found_server(_server_ip, _server_port)
		
		if _server_ip=="" :
			#start discov
			_network_discov = IvmiDiscov.new()
			if _network_mode==NetMode.Server :
				_network_discov.set_server(_server_port)
			add_child(_network_discov)
			_network_discov.connect("found_server",Callable(self,"_on_found_server"))
			_network_discov.connect("timeout",Callable(self,"_on_timeout"))
			_network_discov.start()




func _on_found_server(server_ip, port) :
	match _network_protocol :
		NetProto.Enet :
			if !_is_connected or _server_ip!=server_ip :
				print("IVMI : Connecting to server ", server_ip, " ", port)
				var peer = ENetMultiplayerPeer.new()
				_server_ip=server_ip
				peer.create_client(server_ip, port)
				multiplayer.multiplayer_peer = peer
				multiplayer.connected_to_server.connect(_on_network_ready)
				multiplayer.server_disconnected.connect(_on_network_lost)


func _on_timeout() :
	# We haven't found an existing server
	# If peer, become a server and connect to it
	if _network_mode==NetMode.Peer and _network_server==null:
		_network_mode=NetMode.Server
		_network_discov.set_server(_server_port)
		match _network_protocol :
			NetProto.Enet :
				var peer = ENetMultiplayerPeer.new()
				peer.create_server(_server_port)
				multiplayer.multiplayer_peer = peer
				_on_network_ready()
		print("IVMI : Starting server")

func _on_network_ready() :
	print("IVMI : Network ready")
	_is_connected=true

func _on_network_lost() :
	print("IVMI : Network lost")
	_is_connected=false
	if _server_ip!="" :
		_on_found_server(_server_ip, _server_port)
		
func _on_peer_connected(id : int) :
	print("Peer ", id, " connected")

func _on_peer_disconnected(id : int) :
	print("Peer ", id, " disconnected")

func set_server_ip(ip):
	_server_ip = ip

func set_server_port(port):
	_server_port = port


func _parse_message(msg) :
	#print("received ", msg["address"], " ", msg["args"])
	#"/ivmi/hello_from_pd" :
							#var peer = _osc_discov.get_last_peer()
							##print(_output_address, _output_port)
							#if _osc_output_address!=peer["address"] or _osc_output_port!=peer["port"] :
								#_osc_output_address = peer["address"]
								#_osc_output_port = peer["port"]
								#_osc_to_pd.set_output(_osc_output_address, _osc_output_port)
								#var local_addr = _osc_to_pd.get_local_address()
								#_osc_to_pd.send_msg("/ivmi/hello_from_gd","sf",[local_addr, _input_port])
	var split = msg["address"].split("/")
	if _debug_osc :
			print("IVMI : Got OSC message ", msg["address"])
	if split[1] == "ivmi" :
		if split[2] == "hello_from_pd" :
			send("hello_from_gd","",[])
		elif split[2] == "scene" :
			match split[3] :
				"create" :
					if msg["args"].size()>0 :
						_create_node(msg["args"][0])
				"request" :
					print("IVMI : request scene is not implemented yet ")
					get_tree().call_group("ivmi_nodes", "declare")
				"listen" :
					print("IVMI : listen to scene is not implemented yet")
		else :
			var node = find_child(split[2], true, false)
			if node is  IvmiNode :
				node.parse(split[3], msg["args"])
			elif _debug_osc :
				print("IVMI : Node ", split[2], " not found in scene")


func send(address, tags, args) :
	#print("sending ", address, " ", args)
	var addr = "/ivmi/"+address
	match _pd_mode :
		PdMode.OSC :
			_osc_to_pd.send_msg(addr, tags, args)
		PdMode.LIBPD :
			var split : Array = addr.split("/")
			split.pop_front()
			_gdpd.start_message(args.size()+split.size())
			for s in split :
				_gdpd.add_symbol(s)
			for a in args :
				if a is float :
					_gdpd.add_float(a)
				elif a is String :
					_gdpd.add_symbol(a)
			_gdpd.finish_list("from_gdpd")

#------------Logging--------------------

func recording_start() :
	_recording_state = RecordingState.RECORDING
	_recording_time = Time.get_ticks_msec()
	_recorded_props.clear()

func recording_stop() :
	_recording_state = RecordingState.STOPPED	
	
func recording_play() :
	_recording_state = RecordingState.PLAYING
	_recording_index = 0
	_recording_time = Time.get_ticks_msec()

func recording_save(f) :
	var saved_file = FileAccess.open(f,FileAccess.WRITE)
	if saved_file != null :
	
		for rp in _recorded_props :
			var json_string = JSON.stringify(rp)
			saved_file.store_line(json_string)
		
		saved_file.close()
	else :
		print("IVMI : Could not save recording to file ", f)	

func recording_load(f) :
	if FileAccess.file_exists(f) :
		var loaded_file = FileAccess.open(f,FileAccess.READ)
		_recorded_props.clear()
		while loaded_file.get_position() < loaded_file.get_length():
			var prop_msg = JSON.parse_string(loaded_file.get_line())
			_recorded_props.append(prop_msg)
		loaded_file.close()
	else :
		print("IVMI : Could not load recording from file ", f)	

func is_recording() :
	return _recording_state==RecordingState.RECORDING

func record_property(address, tags, values) :
	var addr = "/ivmi/"+address
	var args = ["set"]
	args.append_array(values)
	var t = Time.get_ticks_msec()-_recording_time
	_recorded_props.append({"time":t, "address":addr, "tags":tags, "args":args})
	
func _recording_playing_done() :
	pass

# ----------Utils----------------

func get_interface() -> XRInterface:
	return _xr_interface

func _create_node(node_name) :
	pass

func _exit_tree ( ):
	pass

func get_xr_mode():
	return _xr_mode

func is_2D() :
	return _is_2D

func vector3_to_array(vec):
	return [vec.x,vec.y,vec.z]

func array_to_vector3(arr):
	if arr.size() == 3:
		return Vector3(arr[0],arr[1],arr[2])
	if arr.size() == 2:
		return Vector3(arr[0],arr[1],0)
	if arr.size() == 1:
		return Vector3(arr[0],0,0)
	return Vector3.ZERO

func basis_to_array(basis : Basis):
	var arrayX = vector3_to_array(basis.x)
	var arrayY = vector3_to_array(basis.y)
	var arrayZ = vector3_to_array(basis.z)
	return [arrayX[0],arrayX[1],arrayX[2],
			arrayY[0],arrayY[1],arrayY[2],
			arrayZ[0],arrayZ[1],arrayZ[2]]

func array_to_basis(arr) -> Basis:
	return Basis(Vector3(arr[0],arr[1],arr[2]),
				 Vector3(arr[3],arr[4],arr[5]),
				 Vector3(arr[6],arr[7],arr[8]))
