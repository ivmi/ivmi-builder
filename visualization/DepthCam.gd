extends "res://addons/ivmi-builder/core/IvmiNode.gd"

var _color_img : Image = Image.new()
var _color_tex : ImageTexture = ImageTexture.new()
var _depth_img : Image = Image.new()
var _depth_tex : ImageTexture = ImageTexture.new()

var _prop_set = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_add_property("update_point", [0,0,0,0,0,0,0,0])
	
	#Create texture that will be passed to shader
	_color_img.create(640,480,false,Image.FORMAT_RGB8)
	_color_tex.create_from_image(_color_img)
	_depth_img.create(640,480,false,Image.FORMAT_RGBF)
	_depth_tex.create_from_image(_depth_img)

	_depth_img.lock()
	for x in range(640) :
		for y in range(480) :
			_depth_img.set_pixel(x, y, Color((x/640.0)*2000.0-1000.0,(y/480.0)*2000.0,0.0))
	_depth_img.unlock()
	
	_color_img.lock()
	for x in range(640) :
		_color_img.set_pixel(x, 320, Color(1.0,1.0,1.0))
		_color_img.set_pixel(x, 321, Color(1.0,1.0,1.0))
		_color_img.set_pixel(x, 322, Color(1.0,1.0,1.0))
	_color_img.unlock()
	
	#Create mesh
	var vertices = PoolVector3Array()
	for x in range(640) :
		for y in range(480) :
			vertices.push_back(Vector3(x, y, 0))
	var arrays = []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = vertices
	# Create the Mesh.
	$MeshInstance.mesh.add_surface_from_arrays(Mesh.PRIMITIVE_POINTS, arrays)
	#$MeshInstance.mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLE_STRIP, arrays)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if _prop_set :
		_color_tex.set_data(_color_img)
		_depth_tex.set_data(_depth_img)
		$MeshInstance.material_override.set_shader_param("coltex", _color_tex)
		$MeshInstance.material_override.set_shader_param("deptex", _depth_tex)
		_prop_set=false

func set_property(prop, vals) :
	.set_property(prop, vals)
	match prop :
		"update_point":
			_color_img.lock()
			_color_img.set_pixel(vals[0], vals[1], Color(vals[5],vals[6],vals[7]))
			_color_img.unlock()
			_depth_img.lock()
			_depth_img.set_pixel(vals[0], vals[1], Color(vals[2],vals[3],vals[4]))
			_depth_img.unlock()
			_prop_set=true
	
