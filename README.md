# IVMI-builder

(work in progress)

IVMI-builder is a free-software framework that facilitates the design of 
Immersive Virtual Musical Instruments. It relies on the Godot game engine
and the PureData audio programming language.


## Prerequisites

* PureData from [https://puredata.info](https://puredata.info)
* Godot 4.x from [https://godotengine.org/](https://godotengine.org/), with [OpenXR activated](https://docs.godotengine.org/en/stable/tutorials/xr/setting_up_xr.html)
* If you want to be able to integrate the audio when releasing your instrument
    (e.g. in a binary or as an app), install the [Gdpd addon](https://gitlab.univ-lille.fr/ivmi/gdpd)

---

## A first example

### Scene creation


### Patch creation


### Design process


### Release

---

## Creating your instrument


### Godot IvmiScene

The root of your scene must extend from the IvmiScene class/script.
Attach a script to it and start it with :

``` python
extends IvmiScene

func _ready() :
	super._ready()


func _process(delta) :
	super._process(delta)
```

Then your scene tree can look like :

* Main (with attached script extending IvmiScene)
	* XROrigin
		* XRCamera
		* XRController
		* MeshInstance

You can also directly attach the script extending IvmiScene to the ARVROrigin
node.

IvmiScene provides a number of settings to help you design immersive instruments
, such as :

* Pd Mode
* Pd Patch
* XR Mode
* ...


### Godot IvmiNode

IVMI-node are the main components of any IVMI instruments. IVMI-node contains IVMI-properties. By inheriting IVMI-node.gd you are able to define a specific behavior/feedback for each IVMI-property change that occur within the node. You can for example write a code that change the size of the node's mesh when the node is selected.

``` python
extends IvmiNode

func _ready() -> void :
    # Call default IvmiNode constructor
    super._ready()
    
    
	# Add custom properties with array of values
	_add_property("selected", [0])

	# [Optionally define a node type]
	_set_ivmi_type("my_node")
    
    # [Properties are sent by default, optionnally deactivate sending]
    _properties["selected"].set_listen(false)
	
    
func _process(delta : float) -> void : 
    # Call default IvmiNode processing, required for updates
    super._process(delta)
    

func _set_property(prop : String, vals : Array) -> void :
	# Call default properties handling
	super._set_property(prop, vals)
	
	# Handle custom properties
	match prop:
        "selected":
            get_node("MeshInstance").scale.x = 2.0*vals[0]+1.0
```


### IvmiProperty

IvmiProperties are composed of :
- A name : A string
- Values : An array of values (floats, ints, bools, strings, ...)

Each property change is sent to Pure Data, 
when their _listen variable is set to true,
which is the case by default.

``` python
# Add a new property
_add_property("radius", [1])
# Retrieve property values
var values = get_property_values("radius")
radius = get_property_value("radius", 0)
# Set a property
set_property("radius",[0.5])
```

### PureData ivmi_scene


### PureData ivmi_node


### PureData ivmi_property

---

## Rendering

### Mono

### SteamVR headsets

TODO

### Android based headset

Use CompabilityGL Rendering Mode 

#### Activating Passthrough

* Add an environment with a custom color background and an alpha value of 0
* Activate Optional or Requested Passthrough in the Android Export configuration
* Toggle passthrough in the IvmiScene XR properties


### Stereoscopic displays


---

## Multi-user Instruments

---


## Using 3D Interaction Techniques


--- 

## Using PureData abstractions


